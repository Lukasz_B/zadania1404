package com.company;

import java.util.Comparator;

public class SortByIndek implements Comparator<Student> {


    @Override
    public int compare(Student o1, Student o2) {
        return Double.compare(o1.getStudentID(),o2.getStudentID());
    }
}
