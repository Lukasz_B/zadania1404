package com.company;

import java.util.*;

public class Student implements Comparable<Student> {

    private String name;
    private String lastName;
    private Integer StudentID;
    private List<ListaZajecEnum> listOfStudentSubjects;
    private Map<ListaZajecEnum, Integer> studentsGrades;
    private Random random = new Random();

    public Student(String name, String lastName){
        this.name = name;
        this.lastName = lastName;
        this.StudentID = random.nextInt((100000-9999) + 1) + 10000;
        this.listOfStudentSubjects = new ArrayList<>();
        this.studentsGrades = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getStudentID() {
        return StudentID;
    }

    public void setStudentID(Integer studentID) {
        StudentID = studentID;
    }

    public List<ListaZajecEnum> getListOfStudentSubjects() {
        return listOfStudentSubjects;
    }

    public void setListOfStudentSubjects(List<ListaZajecEnum> listOfStudentSubjects) {
        this.listOfStudentSubjects = listOfStudentSubjects;
    }

    public Map<ListaZajecEnum, Integer> getStudentsGrades() {
        return studentsGrades;
    }

    public void setStudentsGrades(Map<ListaZajecEnum, Integer> studentsGrades) {
        this.studentsGrades = studentsGrades;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public void addSubjectForStudent(ListaZajecEnum subject){
        //dodajemy do listy przedmiotow
    }

    public void addGrade(ListaZajecEnum subject, Integer subjectGrade){
        //sprawdzanie czy student uczeszcza na dany przedmiot
        //przypisanie oceny do przedmiotu, czyli podanie jej do mapy
        if (listOfStudentSubjects == null) {
            System.out.println("Błąd nie dodano jeszcze przedmiotów.");
            return;
        }

        if (listOfStudentSubjects.contains(subject)) {
            studentsGrades.put(subject, subjectGrade);
        } else {
            System.out.println("Student nie uczęszcza na dany przedmiot.");
        }
    }

    public void showStudentsSubjects(){
        //print whole listOfStudentsSubjects
    }

    /*public Set<Integer> getSetFromMapofGrades(){
        //
    }*/

    public Double getAverageOfStudentGrades(){
        //na mapie mamy wlasciwosci toEntrySet, po niej iterujemy wyciągając wszystkie oceny
        //i wyciągając z nich średnią
        //zwracamy wynik
        return 0.0;
    }

    /*public List<ListaZajecEnum> subjectsWithoutGrade(){
        //wyciagamy wszystkie przedmioty z mapy i zwracamy liste przedmiotow, ktore maja wartosc null
    }
*/
    @Override
    public int compareTo(Student o) {
        return this.lastName.compareTo(o.getLastName());
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", StudentID=" + StudentID +
                ", listOfStudentSubjects=" + listOfStudentSubjects +
                ", studentsGrades=" + studentsGrades +
                ", random=" + random +
                '}';
    }
}
