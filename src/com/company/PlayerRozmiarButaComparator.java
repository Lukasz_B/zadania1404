package com.company;

import java.util.Comparator;

public class PlayerRozmiarButaComparator implements Comparator<Player> {


    @Override
    public int compare(Player o1, Player o2) {
        return Double.compare(o1.getRozmiarButa(), o2.getRozmiarButa());
    }


}
