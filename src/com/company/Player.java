package com.company;

public class Player implements Comparable<Player> {

    private String name;
    private String lastName;
    private Integer age;
    private Integer overall;
    private Integer Wiek;
    private String NazwaKlubu;
    private Integer RozmiarButa;


    public Player(String name, String lastName, Integer age, Integer overall, Integer wiek, String NazwaKlubu, Integer rozmiarButa) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.overall = overall;
        this.Wiek = wiek;
        this.NazwaKlubu = NazwaKlubu;
        this.RozmiarButa = rozmiarButa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getOverall() {
        return overall;
    }

    public String getNazwaKlubu() {
        return NazwaKlubu;
    }

    public void setNazwaKlubu(String NazwaKlubu) {
        NazwaKlubu = NazwaKlubu;
    }

    public Integer getWiek() {
        return Wiek;
    }

    public void setWiek(Integer wiek) {
        Wiek = wiek;
    }

    public Integer getRozmiarButa() {
        return RozmiarButa;
    }

    public void setRozmiarButa(Integer rozmiarButa) {
        RozmiarButa = rozmiarButa;
    }

    public void setOverall(Integer overall) {
        this.overall = overall;

    }

    @Override
    public int compareTo(Player o) {
        return overall-o.getOverall();
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", NazwaKlubu='" + NazwaKlubu + '\'' +
                ", age=" + age +
                ", Wiek=" + Wiek +
                ", overall=" + overall +
                ", RozmiarButa=" + RozmiarButa +
                '}';
    }
}
