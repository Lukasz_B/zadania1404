package com.company;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        //int randomNum = rand.nextInt((max - min) + 1) + min;

        //Random random = new Random();
        /*for (int i = 0; i<100; i++) {
            int randomNum = random.nextInt((10 - 5) + 1) + 5;
            //System.out.println(randomNum);
        }*/

        Student s1 = new Student("Jan", "Kowalski");
        s1.addGrade(ListaZajecEnum.BIOLOGY,5);
        Student s2 = new Student("Marian", "Nowak");
        s2.addGrade(ListaZajecEnum.BIOLOGY_LABS,4);
        Student s3 = new Student("Baska", "AbC");
        s3.addGrade(ListaZajecEnum.MATH,5);
        Student s4 = new Student("Adam", "Franiu");
        Student s5 = new Student("Halyna", "Nosacz");

        s1.addSubjectForStudent(ListaZajecEnum.BIOLOGY);
        s2.addSubjectForStudent(ListaZajecEnum.BIOLOGY_LABS);
        s3.addSubjectForStudent(ListaZajecEnum.ENGLISH);
        s4.addSubjectForStudent(ListaZajecEnum.ENGLISH_LABS);
        s5.addSubjectForStudent(ListaZajecEnum.MATH);

        List<Student> lista2 = new ArrayList<>();
        lista2.add(s1);
        lista2.add(s2);
        lista2.add(s3);
        lista2.add(s4);
        lista2.add(s5);

        Collections.sort(lista2);
        System.out.println("SORTOWANIE 1: " + lista2);


        SortByAverageGrade poOcenach = new SortByAverageGrade();
        Collections.sort(lista2,poOcenach);
        System.out.println("SORTOWANIE PO OCENACH: " + lista2);

        SortByIndek poIdku = new SortByIndek();
        Collections.sort(lista2, poIdku);
        System.out.println("SORTOWANIE PO AJDIKU: " + lista2);

        /*Player p = new Player("Jan","Kowalski", 22, 67, 22,"ManUTD",44);
        Player p1 = new Player("Marian","Nowak", 23, 69, 23, "Real", 45);
        Player p2 = new Player("Kuba","Ziel", 20, 62,20,"Chealse", 46);
        Player p3 = new Player("Dario","Darulski", 27, 75,27,"Barcelona", 43);
        Player p4 = new Player("Mariusz","Fit", 25, 61, 25, "Liverpool", 44);

        List<Player> list = new ArrayList<>();


        list.add(p);
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);



        System.out.println("Before sort: " + list);
        Collections.sort(list);
        System.out.println("After sort po Overall: " + list);
        PlayerRozmiarButaComparator compar = new PlayerRozmiarButaComparator();
        Collections.sort(list,compar);
        System.out.println("After sort po rozmiarze buta: " + list);
        PlayerNazwaKlubuComparator nazwacompar = new PlayerNazwaKlubuComparator();
        Collections.sort(list,nazwacompar);
        System.out.println("After sort po nazwie klubu: " + list);*/

    }
}
